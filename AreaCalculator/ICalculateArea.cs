﻿namespace AreaCalculator
{
    public interface ICalculateArea
    {
        double[] CalculateArea(double[] args);
    }
}